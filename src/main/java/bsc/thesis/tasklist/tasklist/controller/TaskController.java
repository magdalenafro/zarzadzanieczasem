package bsc.thesis.tasklist.tasklist.controller;

import bsc.thesis.tasklist.tasklist.entities.Task;
import bsc.thesis.tasklist.tasklist.exception.TaskNotFoundException;
import bsc.thesis.tasklist.tasklist.model.Status;
import bsc.thesis.tasklist.tasklist.repository.TaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class TaskController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private TaskRepository taskRepository;

    @PostMapping("/tasks")
    public ModelAndView saveToDb(@ModelAttribute Task task) {
        taskRepository.save(task);
        return new ModelAndView("redirect:/tasks");
    }

    @GetMapping("/tasks")
    public ModelAndView all(Model model) {
        model.addAttribute("tasks", taskRepository.findAll());
        return new ModelAndView("all-tasks");
    }

    @GetMapping("/tasks/add")
    public ModelAndView newTask(Model model){
        model.addAttribute("task", new Task());
        model.addAttribute("statuses", Status.values());
        return new ModelAndView ("add-task");
    }

    @GetMapping("/tasks/{id}")
    public ModelAndView one(@PathVariable String id, Model model) {
        model.addAttribute("task", taskRepository.findById(id).orElseThrow(() -> new TaskNotFoundException(id)));
        model.addAttribute("statuses", Status.values());
        return new ModelAndView("edit-task");
    }

    @PutMapping("/tasks/{id}")
    public ModelAndView updateTask(@ModelAttribute Task task, @PathVariable String id) {
        return taskRepository.findById(id)
                .map(taskToSend -> {
                    taskToSend.setName(task.getName() != null ? task.getName() : taskToSend.getName());
                    taskToSend.setDate(task.getDate() != null ? task.getDate() : taskToSend.getDate());
                    taskToSend.setDescription(task.getDescription() != null
                            ? task.getDescription() : taskToSend.getDescription());
                    taskToSend.setStatus(task.getStatus() != null ? task.getStatus() : taskToSend.getStatus());
                    taskRepository.save(taskToSend);
                    return new ModelAndView("redirect:/tasks");
                })
                .orElseGet(() -> {
                    task.setId(id);
                    taskRepository.save(task);
                    return new ModelAndView("redirect:/tasks");
                });
    }

    @GetMapping("/tasks/delete/{id}")
    public ModelAndView deleteTask(@PathVariable String id) {
        taskRepository.deleteById(id);
        return new ModelAndView("redirect:/tasks");
    }

    @GetMapping("contact")
    public ModelAndView contact(){
        return new ModelAndView("contact");
    }
}

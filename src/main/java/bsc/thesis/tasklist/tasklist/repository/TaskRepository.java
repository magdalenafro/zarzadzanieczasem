package bsc.thesis.tasklist.tasklist.repository;

import bsc.thesis.tasklist.tasklist.entities.Task;
import bsc.thesis.tasklist.tasklist.model.Status;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends MongoRepository<Task, String> {
    Optional<Task> findById(String id);

    List<Task> findByStatus(Status status);
}

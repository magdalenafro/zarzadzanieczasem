package bsc.thesis.tasklist.tasklist.entities;

import bsc.thesis.tasklist.tasklist.model.Status;
import lombok.*;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class Task {
    @Id
    private String id;
    private String name;
    private String description;
    private Status status;
    private String date;

}

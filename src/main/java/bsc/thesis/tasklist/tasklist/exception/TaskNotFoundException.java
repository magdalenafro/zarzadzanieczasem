package bsc.thesis.tasklist.tasklist.exception;

public class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException(String id) {
        super("Could not find task with id " + id);
    }
}

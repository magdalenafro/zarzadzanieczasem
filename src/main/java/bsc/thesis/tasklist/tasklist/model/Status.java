package bsc.thesis.tasklist.tasklist.model;

public enum Status {
    TODO, WIP, TEST, DONE
}
